.. SPDX-License-Identifier: CC-BY-4.0

Advanced Topics
===============

Guest types
-----------

The key terms are *HVM* (Hardware Virtual Machine) and *PV*
(Para-Virtualization). There's also a hybrid called *PVH* that sits in
between and attempts to reap the benefits of both PV and HVM.

**HVM** is what most people expect their hypervisor to provide. It creates a
hardware-assisted virtualization boundary with an emulated machine exposed to
the guest. This means the virtualization stack is in charge of exposing every
peripheral expected to exist in a conventional machine.

**PVH** is a stripped down version of HVM, with most emulated devices turned
off (including NICs and hard drives). It requires enlightened guests, but is
typically faster than pure HVM.

**PV** is a creature of its time and was introduced at the same time as the Xen
hypervisor in the early 2000s. It allowed unmodified guests to run on x86 back
when hardware virtualization had not been yet created. A bare PV :term:`domU` is
nowadays fairly uncommon, with the notable exception of :term:`dom0`. We don't
recommend running PV guests for general purpose workloads as they are a lot more
susceptible speculative security attacks than HVM or PVH.

.. warning::

   The Xen Project security team works hard to ensure all guest types are
   as secure as they can be. PV being *less* safe is an inherent property of the
   way PV guests operate.
