Using :term:`xl`
================

.. _xl man page: https://xenbits.xen.org/docs/unstable/man/xl.1.html

:term:`xl` is a minimalistic CLI tool to interact with the hypervisor. The
`xl man page`_, distributed alongside the tool itself, is the current
authoritative documentation about its interface.

If you haven't used :term:`xl` before, keep reading through the
`Getting Started`_ page, as it provides a practical introduction.

If you've used it before and would like to learn more practical examples, jump
over `Configuration file`_. It shows more options than the introductory material,
and provides links to the authoritative documentation of the configuration
files.

If you'd rather see a collection of examples instead then head over `Examples`.
This is a set of typical use cases along with known caveats and fairly
straightforward instructions.

.. note::

   The Xen Project has deprecated various configuration options on paper, while
   still being supported in code. Many online tutorials still use now obsolete
   syntax, so bear in mind that in case of confusion the man pages are the
   authoritative source of truth.

Getting started
---------------

Interactions with the hypervisor are privileged, so the alternatives to use
:term:`xl` are to run as root, or prepend *sudo* to every command. Choose
whatever is typical in your OS. These instructions assume a working graphical
interface in dom0.

First of all, make sure the tool works. The following command outputs
hypervisor information; This includes Xen version, command line arguments, etc.

.. code-block:: none

   xl info

If the output is a few lines with *Permission denid* at the end, then either
you're not running under Xen, you're not running as root, or you're not running
with *sudo*.

With that check done, create your first guest. Create a folder for it (say,
*/path/to/myguest/*). Download the installer ISO for the guest in the folder
as *installer.iso*. Then, create a virtual hard disk for your guest. :term:`xl`
supports a number of formats, but to keep things simple, use *qemu-img* to
create a resizable 10GiB root disk.

.. code-block:: none

   qemu-img create -f qcow2 disk.qcow2 10G

And create a new file in that same folder called *myguest.cfg* with the
following contents.

.. code-block:: none

   name = 'my_favourite_guest'
   type = 'hvm'
   memory = 2048
   vcpus = 2
   serial = 'pty'
   disk = ['format=raw, vdev=xvda, access=w, target=disk.qcow2',
           'format=raw, vdev=xvdb, access=r, devtype=cdrom, target=installer.iso']

Finally, execute the following :term:`xl` command to start a new guest.

.. code-block:: none

   xl create myguest.cfg

The list of running guests can be queried through yet another xl command, so use
it to ensure the guest is actually running.

.. code-block:: none

   xl list

.. warning::

   If at any point later on something goes wrong and you want to kill the guest,
   you can use ``xl destroy my_favourite_guest``. Forceful termination of a
   guest is highly dangerous as its the VM equivalent of pulling the plug of a
   physical machine; the same data corruption considerations apply. You
   typically want to ask the guest itself to turn itself off if at all possible.

After you get a display, go through the installer as you typically would.
Internet should work as long as dom0 is itself connected. After the final
installation step, turn the VM off as we need to remove the ISO from the cfg
file. Afterwards, it should look like so:

.. code-block:: none

   name = 'my_favourite_guest'
   type = 'hvm'
   memory = 512
   vcpus = 2
   vif = ['bridge=xenbr0']
   disk = ['format=raw, vdev=xvda, access=w, target=disk.qcow2']

Create the guest again with ``xl create``, which will now boot into the root disk as a real machine would.

Configuration file
------------------

.. _xl.cfg man page: https://xenbits.xen.org/docs/unstable/man/xl.cfg.5.html
.. _xl disk configuration man page: https://xenbits.xen.org/docs/unstable/man/xl-disk-configuration.5.html
.. _xl networking configuration man page: https://xenbits.xen.org/docs/unstable/man/xl-network-configuration.5.html

The most important step for guest creation is setting up an appropriate *.cfg*
file. `Getting started`_ showed a few basic keys, but there's a lot more. The
`xl.cfg man page`_ contains up-to-date documentation on the format of the file
and all available keys.

.. note::

   The contents of the *disk* and *vif* keys are further described in the
   `xl disk configuration man page`_ and the
   `xl networking configuration man page`_ respectively.

Typical choices are:

  * Guest type: PV or HVM
  * Firmware (HVM-only): SeaBIOS, RomBIOS or OVMF
  * Bootloader (PV-only): Direct boot, pvgrub2 or pygrub

Advanced usage
--------------

TODO

Configuring virtual storage
+++++++++++++++++++++++++++

TODO

Configuring networking
++++++++++++++++++++++

TODO

