.. SPDX-License-Identifier: CC-BY-4.0

Contributing
============

There's 2 ways for you to contribute to the docs:

#. By raising a ticket on *GitLab issues* with a description of the proposed addition or correction.
#. By submitting a GitLab *Merge Request* (MR) with a suggested change.

.. note::
    `Writing guidelines`_ states the writing style we try to maintain.

If you're proposing changes on an MR, you need a local copy of the guide
sources, which is a collection of rST files. You may download them from
`here <https://gitlab.com/xen-project/people/agvallejo/xen-userguide>`_.

Run :code:`make html` to generate the HTML files. This generates the full
guide, with the landing page on :code:`build/html/index.html`. You can use
any valid rST syntax on the sources in :code:`source/*.rst` and re-run the
generator to see the changes.

Writing guidelines
------------------

New additions to the documentation should follow the rules below in order
to keep a consistent style and improve UX.

Glossary consistency
++++++++++++++++++++

1. Use existing terms whenever possible in your writing.
2. If adding a new term is unavoidable, make the definition resilient to changes over time.
3. Don't use several terms to denote the same concept.

Conciseness
+++++++++++

1. Shorten the text as much as possible.
2. Prefer active voice.
3. Prefer imperative mood.
4. Prefer present tense to future tense.
5. Avoid needless embellishments.
6. Don't use avoidable jargon.

Call to Action
++++++++++++++

.. note::
    A *Call to Action* (CTA) is a brief paragraph encouraging the user to
    explore other areas of the guide. It's meant to give users an explicit
    path to follow. i.e: Proposing Y and Z as follow-up reads after
    explaining X.

1. Add a *Next Steps* section at the end of every page.
2. Consider adding a note (as in, :code:`.. note::`) encouraging users to explore another section if it's related to the section at hand.

Next Steps
----------

Consider engaging with the rest of the community. :doc:`community` has a list of
points of contact.
