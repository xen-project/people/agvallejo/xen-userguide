.. SPDX-License-Identifier: CC-BY-4.0

Getting started
===============

Choose a base OS
----------------
You can install the Xen hypervisor in a number of base OSs, including numerous
Linux distributions and some BSD flavours. Typical choices are AlpineLinux,
Debian or FreeBSD; but you can use many others.

.. note::
    An easy rule of thumb is to check your base OS documentation on whether it
    supports running as a Xen :term:`dom0` (not to be confused with a Xen
    :term:`domU`).


Install the required packages
-----------------------------
.. tab:: Debian

   Debian TODO

.. tab:: AlpineLinux

   AlpineLinux TODO

.. tab:: FreeBSD

   FreeBSD TODO

Configure your bootloader
-------------------------
.. tab:: Debian

   Debian TODO

.. tab:: AlpineLinux

   AlpineLinux TODO

.. tab:: FreeBSD

   FreeBSD TODO

Post-install checks
-------------------

Your next boot should start a Xen system. Verify this is the case by running
:code:`xl info` on your terminal, which ought to print hypervisor details into
your screen.

.. warning::
    :code:`xl` is a low-level CLI tool to communicate with the Xen hypervisor
    and requires you to run it with :code:`sudo` or as :code:`root`. Otherwise,
    the tool can't operate.

Next steps
----------

You need to set up a suitable toolstack to create/list/destroy your
:term:`domains<domain>`.

If you're just playing around the easiest way to start creating them is through
the :term:`xl` tool that you've installed as part of this section. See
:doc:`guides/using_xl` to learn how to use it.

Head over the :doc:`administration` page instead if you need a specialised
administration setup.
