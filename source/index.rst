.. SPDX-License-Identifier: CC-BY-4.0

Xen Users Guide
===============

Welcome to the official Xen Users Guide. Every section of the guide walks
you through a different feature so you can use Xen effectively.

.. note::
    We appreciate your help! If you spot errors in the guide, consider
    contributing fixes back. See :doc:`contributing` for indications on
    what to do.

Intended audience
-----------------
This guide teaches users how to use the Xen infrastructure in various
configurations. The expected audience includes sysadmins, software
engineers and enthusiasts that want to *use* Xen.

We encourage developers and contributors to head over the Developers Guide
(TODO: ref) instead in order to find the reference documentation of the Xen
internals.

Where to start
--------------
Feeling lost and don't know where to start?

Every section of the guide gives some suggestions about next steps so that
it's easier to know where to go from where you are. Follow them if in
doubt, or go to the following section if they don't seem to apply to you.

* If **you know very little about Xen**, then head over to :doc:`introduction`
  in order to get a general overview of the sort of problems it can help
  you solve.

* If **you know roughly what Xen can do** and are looking for a way to start using
  it, then head over to :doc:`getting_started` and follow the instructions there.

* If **you are a regular Xen user**, then simply jump ahead to whatever piques
  your interest. Every section ends with a suggestion of *next steps* so you can
  easily see related content.

.. toctree::
   :maxdepth: 2
   :caption: Content
   :hidden:

   introduction.rst
   getting_started.rst
   guides/index.rst
   administration.rst
   advanced.rst
   troubleshooting.rst
   development.rst
   community.rst
   contributing.rst
   glossary.rst
   faq.rst
