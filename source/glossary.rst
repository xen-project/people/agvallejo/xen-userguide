.. SPDX-License-Identifier: CC-BY-4.0

Glossary
========

.. glossary::
    :sorted:

    control domain
      A domain (commonly :term:`dom0`) with permissions to create and manage
      other domains on the system.

    domid
      Numeric identifier of a running :term:`domain`. Both Xen and its toolstack
      use them extensively as unique identifiers on the local Xen instance.

    domain
      A Xen-specific synonym to *guest* or *VM*.

    domU
      One of potentially many unprivileged :term:`domains<domain>` running
      under the hypervisor at any given point in time.

    dom0
      Initial privileged :term:`domain` started at boot time.

    hardware domain
      A domain (commonly :term`dom0`), which shares responsibility with Xen
      about the system as a whole. By default it gets all devices (including all
      disks and network cards), so is responsible for multiplexing guest I/O.

    xl
      A CLI tool that can be used to create, manage and shutdown a
      :term:`domain`.
